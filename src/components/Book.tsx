import React from "react";
import { TBook } from "../types";
import { Button } from "./ui/button";
import { Badge } from "./ui/badge";
import { ArrowRightCircle } from "lucide-react";
import { Link } from "react-router-dom";
import { cn } from "../lib/utils";
import { format } from "date-fns";
import BookingOrCancelButton from "./BookingOrCancelButton";

type BookProps = {
  book: TBook;
} & React.HTMLAttributes<HTMLDivElement>;

const bookPath = (book: TBook) => `/book/${book.id}`;

const formatDateMonthYear = (date: Date) => {
  return format(date, "PP");
};

const Book: React.FC<BookProps> = ({ book, className, ...rest }) => {
  // Render
  return (
    <article
      {...rest}
      className={cn(
        "relative h-[600px] overflow-hidden rounded-2xl rounded-e-2xl shadow-lg",
        className,
      )}
    >
      <div className="relative h-[60%]"></div>
      <img
        className="absolute left-0 top-0 z-0 h-[65%] w-full rounded-t-2xl object-cover"
        src={book.cover}
        alt={book.title}
      />
      <div className="relative z-10 flex h-[40%] flex-col justify-between rounded-t-3xl bg-white px-5 py-3">
        <h2
          title={`${book.title} - ${book.author}`}
          aria-label={`${book.title} - ${book.author}`}
          className="overflow-hidden text-ellipsis whitespace-nowrap p-0 text-2xl font-bold"
        >
          <Link to={bookPath(book)} className="hover:underline">
            {book.title} - {book.author}
          </Link>
        </h2>
        <time className="text-xs" dateTime={book.publishedAt.toString()}>
          {formatDateMonthYear(book.publishedAt)}
        </time>
        <ul className="flex justify-start space-x-2">
          <li>
            <Badge>{book.category}</Badge>
          </li>
        </ul>
        <p
          className="h-12 text-left text-xs"
          aria-label={book.description}
          title={book.description}
        >
          {book.description.substring(0, 100)}
          {book.description.length > 100 && <span className="mx-1">...</span>}
          <Button
            asChild
            className="h-0 space-x-2 p-0 text-xs font-bold underline"
            variant="link"
          >
            <Link to={bookPath(book)}>
              <span>Voir plus</span>
              <ArrowRightCircle className="h-4 w-4" />
            </Link>
          </Button>
        </p>
        {book.currentBooking && (
          <div className="flex flex-col text-xs">
            <span>Réservé par : {book.currentBooking?.userEmail}</span>
            <span>
              Réservation du{" "}
              {formatDateMonthYear(book.currentBooking?.startDate)} au{" "}
              {formatDateMonthYear(book.currentBooking?.endDate)}
            </span>
          </div>
        )}
        <BookingOrCancelButton book={book} />
      </div>
    </article>
  );
};

export default Book;
