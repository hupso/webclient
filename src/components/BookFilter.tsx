import { GetBooksFilter } from "../services/book.service";
import { Input } from "./ui/input";
import { Button } from "./ui/button";
import { FormEvent, useState } from "react";
import { DatePicker } from "./ui/date-picker";
import { Switch } from "./ui/switch";
import { Label } from "./ui/label";

interface BookFilterProps {
  onFilterChange: (filter: GetBooksFilter) => void;
}

const BookFilter = ({ onFilterChange }: BookFilterProps) => {
  const [filter, setFilter] = useState<GetBooksFilter>({
    title: "",
    category: "",
    available: false,
  });

  const handleFilterBooks = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    onFilterChange(filter);
  };

  const handleSetFilter = (
    key: keyof GetBooksFilter,
    value: Date | string | undefined | boolean,
  ) => {
    setFilter((prev) => ({ ...prev, [key]: value }));
  };

  return (
    <div className="h-screen w-96 shrink-0 border-x p-4">
      <form className="space-y-4" onSubmit={handleFilterBooks}>
        <div className="space-y-2">
          <Label htmlFor="books-filter-title">Rechercher un livre</Label>
          <Input
            id="books-filter-title"
            type="text"
            placeholder="Rechercher un livre"
            value={filter.title}
            onChange={(e) => handleSetFilter("title", e.target.value)}
          />
        </div>
        <div className="space-y-2">
          <Label htmlFor="books-filter-category">
            Rechercher une categorie
          </Label>
          <Input
            id="books-filter-category"
            type="text"
            placeholder="Rechercher une categorie"
            value={filter.category}
            onChange={(e) => handleSetFilter("category", e.target.value)}
          />
        </div>
        <div className="space-y-2">
          <Label htmlFor="books-filter-date">
            Rechercher par date de publication
          </Label>
          <DatePicker
            id="books-filter-date"
            className="w-full"
            value={filter.publishedAt || undefined}
            onChange={(date) => handleSetFilter("publishedAt", date)}
            placeholder="Rechercher par date de publication"
          />
        </div>
        <div className="flex items-center space-x-2">
          <Switch
            id="books-filter-available"
            checked={filter.available}
            onCheckedChange={(checked) => handleSetFilter("available", checked)}
          />
          <Label htmlFor="books-filter-available">Livre disponible</Label>
        </div>
        <Button variant="secondary" className="w-full">
          Rechercher
        </Button>
      </form>
    </div>
  );
};

export default BookFilter;
