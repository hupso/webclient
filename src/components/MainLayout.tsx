import { Outlet } from "react-router-dom";

const MainLayout = () => {
  return (
    <div className="container">
      <main>
        <Outlet />
      </main>
    </div>
  );
};

export default MainLayout;
