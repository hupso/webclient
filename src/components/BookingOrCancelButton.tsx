import React from "react";
import { TBook } from "../types";
import BookingDialog from "./BookingDialog";
import { Button } from "./ui/button";
import CancelBookingDialog from "./CancelBookingDialog";
import { cn } from "../lib/utils";

type BookingOrCancelButtonProps = {
  book: TBook;
  className?: string;
};

const BookingOrCancelButton: React.FC<BookingOrCancelButtonProps> = ({
  book,
  className,
}) => {
  // Render

  return (
    <>
      {!book.currentBooking ? (
        <BookingDialog book={book}>
          <Button className={cn("w-full", className)}>Louer</Button>
        </BookingDialog>
      ) : (
        <CancelBookingDialog book={book}>
          <Button variant="outline" className={cn("w-full", className)}>
            Annuler la réservation
          </Button>
        </CancelBookingDialog>
      )}
    </>
  );
};

export default BookingOrCancelButton;
