import { cn } from "../lib/utils";

interface ErrorComponentProps {
  className?: string;
  message: string;
  action: JSX.Element;
}

const ErrorComponent: React.FC<ErrorComponentProps> = ({
  message,
  action,
  className,
}) => {
  return (
    <div
      className={cn(
        "flex h-full w-full flex-col items-center justify-center space-y-2",
        className,
      )}
    >
      <p>{message}</p>
      <div>{action}</div>
    </div>
  );
};

export default ErrorComponent;
