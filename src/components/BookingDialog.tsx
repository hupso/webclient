import { TBook } from "../types";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "./ui/dialog";

import { useBookingBook } from "../services/booking.service";
import { Input } from "./ui/input";
import { DatePicker } from "./ui/date-picker";
import { Button } from "./ui/button";
import { useForm, SubmitHandler } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { z } from "zod";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "./ui/form";

type BookingFormValues = {
  email: string;
  startDate: Date;
  endDate: Date;
};

const bookingValidationSchema = z
  .object({
    email: z.string().email("Email invalide"),
    startDate: z.date({ required_error: "Date de début requise" }),
    endDate: z.date({ required_error: "Date de début requise" }),
  })
  .refine((data) => data.endDate > data.startDate, {
    message: "La date de fin doit être supérieure à la date de début",
    path: ["endDate"],
  });

type BookingDialogProps = {
  open?: boolean;
  onOpen?: (open: boolean) => void;
  book: TBook;
} & React.PropsWithChildren;

const BookingDialog: React.FC<BookingDialogProps> = ({
  book,
  children,
  open,
  onOpen,
}) => {
  // Hooks

  const { mutate, isPending } = useBookingBook();

  const form = useForm<BookingFormValues>({
    resolver: zodResolver(bookingValidationSchema),
    defaultValues: {
      email: "",
      startDate: new Date(),
    },
  });

  // Handlers

  const onSubmit: SubmitHandler<BookingFormValues> = (data) => {
    mutate({
      userEmail: data.email,
      bookId: book.id,
      startDate: data.startDate,
      endDate: data.endDate,
    });
  };

  // Render

  return (
    <Dialog open={open} onOpenChange={onOpen}>
      <DialogTrigger asChild>{children}</DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Louer : {book.title}</DialogTitle>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
            <FormField
              control={form.control}
              name="email"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Email</FormLabel>
                  <FormControl>
                    <Input placeholder="Email" {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="startDate"
              render={({ field }) => (
                <FormItem className="flex flex-col">
                  <FormLabel>Date de début</FormLabel>
                  <FormControl>
                    <DatePicker placeholder="Date de début" {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="endDate"
              render={({ field }) => (
                <FormItem className="flex flex-col">
                  <FormLabel>Date de fin</FormLabel>
                  <FormControl>
                    <DatePicker placeholder="Date de fin" {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <Button type="submit" disabled={isPending}>
              {isPending ? "Chargement..." : "Louer"}
            </Button>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};

export default BookingDialog;
