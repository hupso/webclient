import { Dialog, DialogClose, DialogDescription } from "@radix-ui/react-dialog";
import React from "react";
import {
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "./ui/dialog";
import { useCancelBooking } from "../services/booking.service";
import { TBook } from "../types";
import { useToast } from "./ui/use-toast";
import { Button } from "./ui/button";

type CancelBookingDialogProps = {
  book: TBook;
} & React.PropsWithChildren;

const CancelBookingDialog: React.FC<CancelBookingDialogProps> = ({
  book,
  children,
}) => {
  // Hooks

  const { toast } = useToast();
  const { mutate, isPending } = useCancelBooking();

  // Handlers

  const handleCancelBooking = () => {
    if (!book.currentBooking) {
      toast({
        variant: "destructive",
        description: "Impossible de trouver la réservation du livre",
      });
      return;
    }

    mutate({
      bookingId: book.currentBooking.id,
      bookId: book.id,
    });
  };

  // Render

  return (
    <Dialog>
      <DialogTrigger asChild>{children}</DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Annulation de réservation : {book.title}</DialogTitle>
          <DialogDescription>
            Etes-vous sur de vouloir annuler la réservation ?
          </DialogDescription>
        </DialogHeader>
        <div className="flex justify-between space-x-2">
          <Button onClick={handleCancelBooking} disabled={isPending}>
            {isPending
              ? "Annulation en cours..."
              : "Oui j'annule la réservation"}
          </Button>
          <DialogClose asChild>
            <Button variant="ghost">Non je préserve ma réservation</Button>
          </DialogClose>
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default CancelBookingDialog;
