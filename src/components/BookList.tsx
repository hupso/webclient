import React from "react";
import { TBook } from "../types";
import Book from "./Book";

type BookListProps = {
  books: TBook[];
};

const BookList: React.FC<BookListProps> = ({ books }) => {
  return (
    <div>
      {books?.length === 0 ? (
        <div className="flex h-screen items-center justify-center">
          <p>Aucun livre trouvé</p>
        </div>
      ) : (
        <div className="grid h-screen grow grid-cols-2 gap-4 overflow-y-scroll p-4">
          {books?.map((book) => <Book key={book.id} book={book} />)}
        </div>
      )}
    </div>
  );
};

export default BookList;
