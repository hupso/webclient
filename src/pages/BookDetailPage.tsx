import { Link, useParams } from "react-router-dom";
import { useGetBookDetail } from "../services/book.service";
import { Button } from "../components/ui/button";
import { Badge } from "../components/ui/badge";
import { HttpStatusCode } from "axios";
import ErrorComponent from "../components/ErrorComponent";
import Spinner from "../components/Spinner";
import BookingOrCancelButton from "../components/BookingOrCancelButton";

const BookDetailPage = () => {
  // Hooks

  const { bookId = "" } = useParams();

  const { data: book, status: bookStatus, error } = useGetBookDetail(bookId);

  // Render

  if (bookStatus === "pending") {
    return (
      <div className="flex h-screen w-full flex-col items-center justify-center">
        <Spinner className="h-20 w-20" />
      </div>
    );
  }

  if (bookStatus === "error") {
    if (error.response?.status === HttpStatusCode.NotFound) {
      return (
        <div className="flex h-screen items-center justify-center">
          <ErrorComponent
            message="Livre non trouvé"
            action={
              <Button asChild>
                <Link to="/">Retour à la liste des livres</Link>
              </Button>
            }
          />
        </div>
      );
    } else {
      return (
        <div className="flex h-screen items-center justify-center">
          <ErrorComponent
            message="Une erreur est survenue"
            action={
              <Button asChild>
                <Link to="/">Retour à la liste des livres</Link>
              </Button>
            }
          />
        </div>
      );
    }
  }

  return (
    <div className="flex flex-col items-center justify-center p-6">
      <Button asChild className="mb-6">
        <Link to="/">Retour</Link>
      </Button>
      <img
        className="h-96 w-64 rounded-lg object-cover shadow-lg"
        src={book.cover}
        alt={book.title}
      />
      <h1 className="mt-6 text-3xl font-bold">{book.title}</h1>
      <h2 className="mt-2 text-xl">{book.author}</h2>
      <Badge className="mt-2">{book.category}</Badge>
      <p className="mt-4 text-center">{book.description}</p>
      <BookingOrCancelButton book={book} className="mt-4 w-96" />
    </div>
  );
};

export default BookDetailPage;
