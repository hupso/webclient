import { GetBooksFilter, useGetBooks } from "../services/book.service";
import { useEffect, useState } from "react";
import BookFilter from "../components/BookFilter";
import Spinner from "../components/Spinner";
import ErrorComponent from "../components/ErrorComponent";
import { Button } from "../components/ui/button";
import BookList from "../components/BookList";

const BooksPage = () => {
  const [currentFilter, setCurrentFilter] = useState<GetBooksFilter>({
    title: "",
    category: "",
    publishedAt: null,
  });

  const {
    data: books,
    status: booksStatus,
    refetch,
  } = useGetBooks(currentFilter);

  // Effects

  useEffect(() => {
    refetch();
  }, [currentFilter]);

  // Render

  return (
    <div className="flex">
      <BookFilter onFilterChange={setCurrentFilter} />
      <div className="h-screen w-screen">
        {booksStatus === "pending" ? (
          <div className="flex h-screen w-full flex-col items-center justify-center">
            <Spinner className="h-20 w-20" />
          </div>
        ) : booksStatus === "error" ? (
          <ErrorComponent
            message="Une erreur est survenue"
            action={<Button onClick={() => refetch()}>Réessayer</Button>}
          />
        ) : (
          <BookList books={books} />
        )}
      </div>
    </div>
  );
};

export default BooksPage;
