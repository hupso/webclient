import axios, { AxiosError } from "axios";
import { config } from "../config";

export const apiClient = axios.create({
  baseURL: config.apiUrl,
  headers: {
    "Content-Type": "application/json",
  },
});

export type ApiError = AxiosError;
