export type TBook = {
  id: string;
  cover: string;
  title: string;
  description: string;
  author: string;
  publishedAt: Date;
  category: string;
  currentBooking?: TBooking;
};

export type TBooking = {
  id: string;
  userEmail: string;
  startDate: Date;
  endDate: Date;
};
