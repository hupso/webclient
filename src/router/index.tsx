import { createBrowserRouter } from "react-router-dom";
import MainLayout from "../components/MainLayout";
import BooksPage from "../pages/BooksPage";
import BookDetailPage from "../pages/BookDetailPage";

export const router = createBrowserRouter([
  {
    element: <MainLayout />,
    children: [
      {
        path: "/",
        element: <BooksPage />,
      },
      {
        path: "/book/:bookId",
        element: <BookDetailPage />,
      },
    ],
  },
]);
