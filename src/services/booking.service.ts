import { useMutation } from "@tanstack/react-query";
import { HttpStatusCode } from "axios";
import { useToast } from "../components/ui/use-toast";
import { ApiError, apiClient } from "../lib/api";
import { queryClient } from "../lib/react-query";
import { TBook } from "../types";
import { BookServiceQueryKeys } from "./book.service";

export type BookingBookParams = {
  userEmail: string;
  bookId: string;
  startDate: Date;
  endDate: Date;
};

// TODO: Normalement on devrait booker un livre avec un utilisateur connecté en passant son token dans le header plutot que dans le body
const bookingBook = async (payload: BookingBookParams): Promise<TBook> => {
  const { data } = await apiClient.post(`/booking`, payload);
  return data;
};

export const useBookingBook = () => {
  const { toast } = useToast();

  return useMutation<TBook, ApiError, BookingBookParams>({
    mutationFn: bookingBook,
    onSuccess: (_, variables) => {
      toast({
        description: "Vous avez bien réservé ce livre",
      });
      queryClient.invalidateQueries({ queryKey: [BookServiceQueryKeys.books] });
      queryClient.invalidateQueries({
        queryKey: BookServiceQueryKeys.book(variables.bookId),
      });
    },
    onError: (error) => {
      if (error.response?.status === HttpStatusCode.Conflict) {
        return toast({
          variant: "destructive",
          description: "Ce livre est déjà réservé pour ces dates",
        });
      }

      toast({
        variant: "destructive",
        description: "Une erreur est survenue lors de la réservation du livre",
      });
    },
  });
};

export type CancelBookingParams = {
  bookingId: string;
  bookId: string;
};

const cancelBooking = async (payload: CancelBookingParams): Promise<void> => {
  await apiClient.delete(`/booking/${payload.bookingId}`);
};

export const useCancelBooking = () => {
  const { toast } = useToast();

  return useMutation<void, ApiError, CancelBookingParams>({
    mutationFn: cancelBooking,
    onSuccess: (_, variables) => {
      toast({
        description: "Vous avez bien annulé votre réservation",
      });
      queryClient.invalidateQueries({ queryKey: [BookServiceQueryKeys.books] });
      queryClient.invalidateQueries({
        queryKey: BookServiceQueryKeys.book(variables.bookId),
      });
    },
    onError: () => {
      toast({
        variant: "destructive",
        description:
          "Une erreur est survenue lors de l'annulation de la réservation",
      });
    },
  });
};
