import { useQuery } from "@tanstack/react-query";
import { ApiError, apiClient } from "../lib/api";
import { TBook } from "../types";

export type GetBooksFilter = {
  title?: string;
  category?: string;
  publishedAt?: Date | null;
  available?: boolean;
};

const STALE_TIME = 1000 * 60 * 5; // 5 minutes

export const BookServiceQueryKeys = {
  books: "books",
  book: (id: string) => ["book", id],
};

const fetchBooks = async (filter?: GetBooksFilter): Promise<TBook[]> => {
  const { data } = await apiClient.get("/book", {
    params: {
      ...filter,
      available: filter?.available === true ? 1 : 0,
    },
  });
  return data;
};

export const useGetBooks = (filter?: GetBooksFilter) => {
  return useQuery<TBook[], ApiError>({
    queryKey: [BookServiceQueryKeys.books, filter],
    queryFn: () => fetchBooks(filter),
    staleTime: STALE_TIME,
  });
};

const fetchBookDetail = async (id: string): Promise<TBook> => {
  const { data } = await apiClient.get(`/book/${id}`);
  return data;
};

export const useGetBookDetail = (id: string) => {
  return useQuery<TBook, ApiError>({
    queryKey: BookServiceQueryKeys.book(id),
    queryFn: () => fetchBookDetail(id),
    staleTime: STALE_TIME,
  });
};
